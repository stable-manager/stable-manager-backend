# Backend part of Stable Manager application

#### General information 

Stable Manager is an application for instructors and trainers. This project allows us to manage data about clients (riders) who take part in horse riding lessons or similar events like horse competitions.
You can add, update or delete a rider and use the search panel which is a fast and easy way to find a person. When you start to use the app, all your information and customer data will be stored in a connected database.
Stable Manager is a very easy and helpful solution to use.

# Technologies used

| Name | Version |
| ------ | ------ |
| Spring Web | 2.6.3 |
| Spring Data JPA | 2.6.3 |
| MySQL connector | 8.0.25|
| Apache Log4j | 2.17.1 |
| Junit | 4.3.12 |
| TestContainers | 1.16.3 |


# Frontend part of Stable Manager application 
Click and check a demo and more information about user interface: [stable-manager-frontend](https://gitlab.com/stable-manager/stable-manager-frontend)

