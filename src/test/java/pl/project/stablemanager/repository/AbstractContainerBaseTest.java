package pl.project.stablemanager.repository;

import org.junit.jupiter.api.AfterAll;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@ActiveProfiles("test")
@Testcontainers
public class AbstractContainerBaseTest {

    @Container
    static MySQLContainer mySQLContainer = (MySQLContainer) new MySQLContainer("mysql:8.0.25")
            .withDatabaseName("test");

    static {
        mySQLContainer.start();
    }

    @AfterAll
    public static void stop() {
        mySQLContainer.stop();
    }

}
