package pl.project.stablemanager.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import pl.project.stablemanager.model.Rider;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class RiderRepositoryIntegrationTest extends AbstractContainerBaseTest {

    @Autowired
    RiderRepository riderRepository;

    @Test
    public void should_find_all_riders_in_db() {
        List<Rider> riderList = riderRepository.findAll();

        assertThat(riderList).isNotNull();
        assertThat(riderList.size()).isEqualTo(2);
        assertThat(riderList.get(0).getFullName()).isEqualTo("test-rider1");
        assertThat(riderList.get(1).getFullName()).isEqualTo("test-rider2");
    }

    @Test
    public void should_find_a_rider_by_id() {
        Optional<Rider> riderById = riderRepository.findRiderById(1L);

        assertThat(riderById.get()).isNotNull();
        assertThat(riderById.get().getFullName()).isEqualTo("test-rider1");
    }

    @Test
    public void should_add_a_new_rider() {
        Rider rider = new Rider("Jan Kowalski", "jkowalski@gmail.com", "beginner", "1234", "url");
        Rider savedRider = riderRepository.save(rider);

        assertThat(savedRider.getId()).isNotNull();
        assertThat(rider).usingRecursiveComparison().ignoringFields("id").isEqualTo(savedRider);
    }

    @Test
    public void should_delete_a_rider_by_id() {
        riderRepository.deleteById(1L);

        assertThat(riderRepository.findRiderById(1L)).isNotPresent();
        assertThat(riderRepository.findAll().size()).isEqualTo(1);
    }
}
