package pl.project.stablemanager.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;
import pl.project.stablemanager.dto.RiderDto;
import pl.project.stablemanager.dto.RiderDtoMapper;
import pl.project.stablemanager.dto.RiderMapper;
import pl.project.stablemanager.exception.RiderNotFoundException;
import pl.project.stablemanager.model.Rider;
import pl.project.stablemanager.repository.RiderRepository;
import java.util.List;
import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
class RiderServiceUnitTest {

    @Mock
    RiderRepository riderRepository;
    @Mock
    RiderDtoMapper riderDtoMapper;
    @Mock
    RiderMapper riderMapper;

    RiderService riderService;

    @BeforeEach
    public void test() {
        riderService = new RiderService(riderRepository, riderDtoMapper, riderMapper);
    }

    @Test
    public void should_find_rider_by_id() {
        Rider rider = mock(Rider.class);
        RiderDto riderDto = mock(RiderDto.class);

        Mockito.when(riderRepository.findRiderById(rider.getId())).thenReturn(Optional.ofNullable(rider));
        Mockito.when(riderDtoMapper.mapToRiderDto(rider)).thenReturn(riderDto);

        RiderDto riderById = riderService.findRiderById(rider.getId());

        assertThat(riderById).isEqualTo(riderDto);
    }

    @Test
    public void should_return_throw_when_trying_to_find_a_rider_who_does_not_exist_in_db() {
        Mockito.when(riderRepository.findRiderById(1L)).thenReturn(Optional.empty());

        Assertions.assertThrows(RiderNotFoundException.class, () -> riderService.findRiderById(1L));
    }

    @Test
    public void should_add_a_new_rider() {
        Rider rider = mock(Rider.class);
        RiderDto riderDto = mock(RiderDto.class);

        Mockito.when(riderMapper.mapToRider(riderDto)).thenReturn(rider);
        Mockito.when(riderRepository.save(rider)).thenReturn(rider);
        Mockito.when(riderDtoMapper.mapToRiderDto(rider)).thenReturn(riderDto);

        RiderDto addedRider = riderService.addRider(riderDto);

        assertThat(addedRider).isEqualTo(riderDto);
    }

    @Test
    public void should_update_a_rider_in_db() {
        Rider rider = mock(Rider.class);
        RiderDto riderDto = mock(RiderDto.class);

        Mockito.when(riderRepository.findRiderById(riderDto.getId())).thenReturn(Optional.ofNullable(rider));

        riderService.updateRider(riderDto);

        assertThat(rider.getFullName()).isEqualTo(riderDto.getFullName());
    }

    @Test
    public void should_return_throw_when_trying_to_update_a_rider_who_does_not_exist_in_db() {
        RiderDto riderDto = mock(RiderDto.class);

        Assertions.assertThrows(ResponseStatusException.class, () -> riderService.updateRider(riderDto));
    }

    @Test
    public void should_find_all_riders_in_db() {
        Rider firstRiderFromDb = mock(Rider.class);
        Rider secondRiderFromDb = mock(Rider.class);

        RiderDto firstRiderFromDbDto = mock(RiderDto.class);
        RiderDto secondRiderFromDbDto = mock(RiderDto.class);

        Mockito.when(riderRepository.findAll()).thenReturn(List.of(firstRiderFromDb, secondRiderFromDb));
        Mockito.when(riderDtoMapper.mapToRiderDto(firstRiderFromDb)).thenReturn(firstRiderFromDbDto);
        Mockito.when(riderDtoMapper.mapToRiderDto(secondRiderFromDb)).thenReturn(secondRiderFromDbDto);

        List<RiderDto> allRidersFromDb = riderService.findAllRiders();

        assertThat(allRidersFromDb).isNotNull();
        assertThat(allRidersFromDb.size()).isEqualTo(2);
        assertThat(allRidersFromDb.get(0)).isEqualTo(firstRiderFromDbDto);
        assertThat(allRidersFromDb.get(1)).isEqualTo(secondRiderFromDbDto);
    }

    @Test
    public void should_return_empty_list_when_riders_do_not_exist_in_db() {
        Mockito.when(riderRepository.findAll()).thenReturn(List.of());

        List<RiderDto> allRidersFromDb = riderService.findAllRiders();

        assertThat(allRidersFromDb).isEmpty();
    }

    @Test
    public void should_delete_rider_in_db() {
        Rider rider = new Rider("test1", "test1", "test1", "test1", "test1");

        Mockito.when(riderRepository.findRiderById(1L)).thenReturn(Optional.of(rider));

        riderService.deleteRider(1L);
    }

    @Test
    public void should_return_throw_when_trying_to_delete_a_nonexistent_rider() {
        Mockito.when(riderRepository.findRiderById(1L)).thenReturn(Optional.empty());

        Assertions.assertThrows(ResponseStatusException.class, () -> riderService.deleteRider(1L));
    }
}
