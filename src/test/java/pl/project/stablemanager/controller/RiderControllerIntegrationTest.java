package pl.project.stablemanager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.project.stablemanager.model.Rider;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class RiderControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void should_return_all_riders_in_db() throws Exception {
        mockMvc.perform(get("/riders"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.size()", Matchers.is(2)));
    }

    @Test
    public void should_return_the_rider_in_db() throws Exception {
         mockMvc.perform(get("/riders/1"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.id", Matchers.is(1)))
                .andExpect(jsonPath("$.fullName", Matchers.is("test-rider1")))
                .andExpect(jsonPath("$.email", Matchers.is("test1")))
                .andExpect(jsonPath("$.ridingSkills", Matchers.is("test1")))
                .andExpect(jsonPath("$.phoneNumber", Matchers.is("1234")))
                .andExpect(jsonPath("$.imageUrl", Matchers.is("test1")));
    }

    @Test
    public void should_add_new_rider_in_the_database() throws Exception {
        mockMvc.perform(post("/riders")
                        .content(objectMapper.writeValueAsString(new Rider("Jan Kowalski", "jkowalski@gmail.com", "beginner", "1234", "url")))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.id", Matchers.is(3)));
    }

    @Test
    public void should_delete_rider_in_db() throws Exception {
        mockMvc.perform(delete("/riders/2"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    public void should_update_rider_in_db() throws Exception {
        Rider rider = new Rider("Jan Kowalski", "jkowalski@gmail.com", "beginner", "1234", "url");
        rider.setId(1L);

        mockMvc.perform(put("/riders")
                        .content(objectMapper.writeValueAsString(rider))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.fullName", Matchers.is(rider.getFullName())));
    }
}
