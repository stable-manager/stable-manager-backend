package pl.project.stablemanager.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Rider implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;
    private String fullName;
    private String email;
    private String ridingSkills;
    private String phoneNumber;
    private String imageUrl;

    public Rider() {
    }

    public Rider(String fullName, String email, String ridingSkills, String phoneNumber, String imageUrl) {
        this.fullName = fullName;
        this.email = email;
        this.ridingSkills = ridingSkills;
        this.phoneNumber = phoneNumber;
        this.imageUrl = imageUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRidingSkills() {
        return ridingSkills;
    }

    public void setRidingSkills(String ridingSkills) {
        this.ridingSkills = ridingSkills;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rider rider = (Rider) o;
        return Objects.equals(id, rider.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Rider{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", email='" + email + '\'' +
                ", ridingSkills='" + ridingSkills + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
