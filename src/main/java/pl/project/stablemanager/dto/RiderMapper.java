package pl.project.stablemanager.dto;

import org.springframework.stereotype.Service;
import pl.project.stablemanager.model.Rider;

@Service
public class RiderMapper {

    public Rider mapToRider(RiderDto riderDto){
        Rider rider = new Rider();
        rider.setId(riderDto.getId());
        rider.setFullName(riderDto.getFullName());
        rider.setEmail(riderDto.getEmail());
        rider.setRidingSkills(riderDto.getRidingSkills());
        rider.setPhoneNumber(riderDto.getPhoneNumber());
        rider.setImageUrl(riderDto.getImageUrl());
        return rider;
    }
}
