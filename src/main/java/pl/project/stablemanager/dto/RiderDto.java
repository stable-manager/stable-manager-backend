package pl.project.stablemanager.dto;

public class RiderDto {

    private Long id;
    private String fullName;
    private String email;
    private String ridingSkills;
    private String phoneNumber;
    private String imageUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRidingSkills() {
        return ridingSkills;
    }

    public void setRidingSkills(String ridingSkills) {
        this.ridingSkills = ridingSkills;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
