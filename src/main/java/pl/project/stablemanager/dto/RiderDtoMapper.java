package pl.project.stablemanager.dto;

import org.springframework.stereotype.Service;
import pl.project.stablemanager.model.Rider;

@Service
public class RiderDtoMapper {

    public RiderDto mapToRiderDto(Rider rider){
        RiderDto riderDto = new RiderDto();
        riderDto.setId(rider.getId());
        riderDto.setFullName(rider.getFullName());
        riderDto.setEmail(rider.getEmail());
        riderDto.setRidingSkills(rider.getRidingSkills());
        riderDto.setPhoneNumber(rider.getPhoneNumber());
        riderDto.setImageUrl(rider.getImageUrl());
        return riderDto;
    }
}
