package pl.project.stablemanager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import pl.project.stablemanager.dto.RiderDto;
import pl.project.stablemanager.dto.RiderDtoMapper;
import pl.project.stablemanager.dto.RiderMapper;
import pl.project.stablemanager.exception.RiderNotFoundException;
import pl.project.stablemanager.model.Rider;
import pl.project.stablemanager.repository.RiderRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RiderService {

    private final RiderRepository riderRepository;
    private final RiderDtoMapper riderDtoMapper;
    private final RiderMapper riderMapper;

    @Autowired
    public RiderService(RiderRepository riderRepository, RiderDtoMapper riderDtoMapper, RiderMapper riderMapper) {
        this.riderRepository = riderRepository;
        this.riderDtoMapper = riderDtoMapper;
        this.riderMapper = riderMapper;
    }

    public RiderDto addRider(RiderDto riderDto) {
        Rider rider = riderMapper.mapToRider(riderDto);
        Rider savedRider = riderRepository.save(rider);
        return riderDtoMapper.mapToRiderDto(savedRider);
    }

    public RiderDto findRiderById(Long id) {
        return riderRepository.findRiderById(id)
                .map(riderDtoMapper::mapToRiderDto)
                .orElseThrow(() -> new RiderNotFoundException("Rider by id " + id + " was not found"));
    }

    public List<RiderDto> findAllRiders() {
        return riderRepository.findAll()
                .stream()
                .map(riderDtoMapper::mapToRiderDto)
                .collect(Collectors.toList());
    }

    public RiderDto updateRider(RiderDto riderDto) {
        Rider foundRider = riderRepository.findRiderById(riderDto.getId())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Rider by id " + riderDto.getId() + " was not found"));
        foundRider.setFullName(riderDto.getFullName());
        foundRider.setEmail(riderDto.getEmail());
        foundRider.setRidingSkills(riderDto.getRidingSkills());
        foundRider.setPhoneNumber(riderDto.getPhoneNumber());
        foundRider.setImageUrl(riderDto.getImageUrl());

        Rider updatedRider = riderRepository.save(foundRider);

        return riderDtoMapper.mapToRiderDto(updatedRider);
    }

    public void deleteRider(Long id) {
        riderRepository.findRiderById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Rider by id " + id + " was not found"));
        riderRepository.deleteById(id);
    }
}
