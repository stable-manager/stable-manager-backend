package pl.project.stablemanager.controller;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.project.stablemanager.dto.RiderDto;
import pl.project.stablemanager.service.RiderService;
import java.util.List;

@RestController
@RequestMapping("/riders")
public class RiderController {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(RiderController.class);
    private final RiderService riderService;

    @Autowired
    public RiderController(RiderService riderService) {
        this.riderService = riderService;
    }

    @GetMapping()
    public ResponseEntity<List<RiderDto>> getAllRiders(){
        List<RiderDto> riders = riderService.findAllRiders();
        log.info("{}", riders);
        return new ResponseEntity<>(riders, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<RiderDto> getRiderById(@PathVariable("id") Long id){
        RiderDto riderById = riderService.findRiderById(id);
        log.info("{}", riderById);
        return new ResponseEntity<>(riderById, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<RiderDto> addRider(@RequestBody RiderDto riderDto){
        RiderDto newRider = riderService.addRider(riderDto);
         log.info("{}", newRider);
        return new ResponseEntity<>(newRider, HttpStatus.CREATED);
    }

    @PutMapping()
    public ResponseEntity<RiderDto> updateRider(@RequestBody RiderDto riderDto) {
        RiderDto updateRider = riderService.updateRider(riderDto);
        log.info("{}", updateRider);
        return new ResponseEntity<>(updateRider, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteRider(@PathVariable("id") Long id) {
        riderService.deleteRider(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
