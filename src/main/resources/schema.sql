CREATE TABLE IF NOT EXISTS rider(
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    full_name VARCHAR(255),
    email VARCHAR(255),
    riding_skills VARCHAR(255),
    phone_number VARCHAR(255),
    image_url VARCHAR(255)
);



