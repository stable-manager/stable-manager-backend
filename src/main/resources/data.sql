INSERT INTO
    rider (full_name, email, riding_skills, phone_number, image_url)
    VALUES ('Alicja Sień', 'alasien@op.pl', 'advance', '657 746 737', 'https://images.unsplash.com/photo-1606844763161-d1091276b18e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=688&q=80'),
    ('Magda Kwiat', 'mkwiat1@onet.pl', 'advance', '699 246 937', 'https://images.unsplash.com/photo-1535521204257-30abc396f664?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=688&q=80'),
    ('Maja Lotos', 'lotos1@op.pl', 'advance', '622 345 934', 'https://images.unsplash.com/photo-1571840070793-3462014dfdbd?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDJ8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60'),
    ('Adam Rot', 'arot@gmail.com', 'advance', '688 999 123', 'https://images.unsplash.com/photo-1620063849392-70479c85ad73?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDN8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60'),
    ('Anna Kot', 'kot345@gmail.com', 'beginner', '690 089 123', 'https://images.unsplash.com/photo-1571840070800-668e57c7f7fa?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80'),
    ('Ela Narada', 'elkan@gmail.com', 'advance', '677 567 173', 'https://images.unsplash.com/photo-1590983842881-2fd9672f6c18?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80'),
    ('Iga Pilon', 'xpilon@gmail.com', 'beginner', '655 399 123', 'https://images.unsplash.com/photo-1594885837887-4695e725afdd?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80'),
    ('Anna Nowak', 'nowak@gmail.com', 'beginner', '567 912 333', 'https://images.unsplash.com/photo-1604340737638-0f690ea1543d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=685&q=80');


